/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkOSPRayVolumeMapperNode.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

*/
// .NAME vtkOSPRayVolumeMapperNode - links vtkVolumeMapper  to OSPRay
// .SECTION Description
// Translates vtkVolumeMapper state into OSPRay rendering calls

#ifndef vtkOSPRayVolumeMapperNode_h
#define vtkOSPRayVolumeMapperNode_h

#include "vtkRenderingOSPRayModule.h" // For export macro
#include "vtkVolumeMapperNode.h"

#include <map>

class vtkVolume;

namespace osp
{
  struct TransferFunction;
  struct Volume;
  struct Data;
}

class vtkOSPRayVolumeCacheEntry
{
public:
  vtkOSPRayVolumeCacheEntry()
  {
    Volume = nullptr;
    VoxelData = nullptr;
  }
  osp::Volume* Volume;
  osp::Data *VoxelData;
  vtkTimeStamp BuildTime;
};



class VTKRENDERINGOSPRAY_EXPORT vtkOSPRayVolumeMapperNode :
  public vtkVolumeMapperNode
{
public:
  static vtkOSPRayVolumeMapperNode* New();
  vtkTypeMacro(vtkOSPRayVolumeMapperNode, vtkVolumeMapperNode);
  void PrintSelf(ostream& os, vtkIndent indent);
  //Description:
  //Traverse graph in ospray's prefered order and render
  virtual void Render(bool prepass);
  void SetSamplingRate(double rate) { this->SamplingRate = rate; }
  double GetSamplingRate(double rate) { return this->SamplingRate; }

protected:
  vtkOSPRayVolumeMapperNode();
  ~vtkOSPRayVolumeMapperNode();

  class vtkOSPRayVolumeCacheEntry
  {
  public:
    vtkOSPRayVolumeCacheEntry()
    {
      Volume = nullptr;
      VoxelData = nullptr;
    }
    osp::Volume* Volume;
    osp::Data *VoxelData;
    vtkTimeStamp BuildTime;
  };

  vtkTimeStamp BuildTime;
  vtkTimeStamp PropertyTime;
  osp::TransferFunction* TransferFunction;
  int NumColors;
  std::vector<float> TFVals, TFOVals;
  bool SharedData;
  bool VolumeAdded;
  double SamplingRate;
  std::map< vtkVolume*, std::map< double, vtkOSPRayVolumeCacheEntry* > > Cache;

private:
  vtkOSPRayVolumeMapperNode(const vtkOSPRayVolumeMapperNode&) VTK_DELETE_FUNCTION;
  void operator=(const vtkOSPRayVolumeMapperNode&) VTK_DELETE_FUNCTION;
};
#endif
